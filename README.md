# The Project for ATT


	1. npm install (install dependencies)
	2. npm start (start the project)
	
# The format of Response

## 	comments existed
	{"id":246127311,
	"number":21,
	"title":"Implement/connect error handling",
	"comments":
	[{
		"user":"useless5771",
		"comment":"@gordonwoodhull I managed to provoke this error using the 'Observer demo' example notebook by adding a call to an undefined function. So I have something to begin with, but if you have some other examples that result in such errors they would definitely help me in investigating this issue.\r\n\r\nThanks"}
	]}
	
##    no comment
	{"id":246127311,
	"number":21,
	"title":"Implement/connect error handling",
	"comments":[]
	}
	
	
## avoid Abuse detection problem of Github
### Abuse rate limits
	To protect the quality of service on GitHub, additional rate 
	limits may apply to some actions. 
	For example: rapidly creating content, polling aggressively 
	instead of using webhooks, making API calls with a high 
	concurrency,or repeatedly requesting data that is 
	computationally expensive may result in abuse rate limiting.
	
	
	solution:
	To avoid this problem,I limited the frequency of request data 
	at 60 each time.Each time  executing getEntiredIssues() will 
	send no more than 60 requests,if there are still reqeusts remain
	,execute getEntiredIssues() again until all requests are sent.
	
## project has been deployed in AWS EC2(temporarily)
	https://attwillsun.herokuapp.com/api/issues

