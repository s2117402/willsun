var express=require('express')
var cors=require('cors')
import {requestsHandler} from "./controllers/issues.controller";

const app = express();
app.use(cors());



app.get('/api/issues',requestsHandler);


//error handling
app.use((err, req, res, next) => {
    res.json({
        success: false,
        err: err
    })
});

app.listen(process.env.port || 3000);
console.log("server started");
