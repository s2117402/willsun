import request from 'request-promise-native';
import {requestBodyOfGettingRepos} from "../config/requestBody";
import {requestBodyOfGettingIssues} from "../config/requestBody";
import {requestBodyOfGettingIssueComments} from "../config/requestBody";
import {COUNT} from "../config/github.config";  // github api limit

export function requestsHandler(req, res) {    //handle requests
    getAllRepos().then(allRepos => {
        return collectIssuesWithoutComments(allRepos);
    }).then(issuesWithoutComments => {
        let issues = [];
        issuesWithoutComments.map(issueArray => {
            issues = issues.concat(JSON.parse(issueArray));
        });
        getEntiredIssues(issues, res, 0, []);
    });
}
function getAllRepos() {                              //get all repos
    return request(requestBodyOfGettingRepos());
}

function collectIssuesWithoutComments(Repos) {       //get all issues without comment inside
    var repos = JSON.parse(Repos);
    var promiseListOfRequestIssue = [];
    repos.map(repo => {
        promiseListOfRequestIssue.push(request(requestBodyOfGettingIssues(repo.name)));
    });

    return Promise.all(promiseListOfRequestIssue);
}


function getEntiredIssues(issueswithoutcomments, res, start, issueswithcomments) {    //get final issues with comments
    let end = (start + COUNT > issueswithoutcomments.length) ? issueswithoutcomments.length : (start + COUNT);
    //send request recursively to avoid  abuse detection,each time send no more than 60 requests

    var promiseListOfrequestComments = [];
    issueswithoutcomments.slice(start, end).map(eachIssueWithoutComments => {
        promiseListOfrequestComments.push(new Promise((resolve, reject) => {
            request(
                requestBodyOfGettingIssueComments(eachIssueWithoutComments.comments_url)
            ).then(comments => {
                var allComments = JSON.parse(comments);
                var commentsByIssue = [];
                allComments.map(eachcomment => {
                    commentsByIssue.push({
                        user: eachcomment.user.login,
                        comment: eachcomment.body
                    })
                })
                resolve({
                    id: eachIssueWithoutComments.id,
                    number: eachIssueWithoutComments.number,
                    title: eachIssueWithoutComments.title,
                    comments: commentsByIssue
                });
            });
        }));

    });

    return Promise.all(promiseListOfrequestComments)
        .then((issues) => {
            issueswithcomments = issueswithcomments.concat(issues);
            if(end < issueswithoutcomments.length) {  //find request remain,execute getEntiredIssues() again to send requests
                getEntiredIssues(issueswithoutcomments, res, end, issueswithcomments);
            } else {
                res.json(issueswithcomments);
            }
        });
}

