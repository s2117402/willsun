
var token='token 88dc2a0a9502b46fd33834c5e6e0c5ba3e6a34dd';
export function requestBodyOfGettingRepos(){
    return({
        url: 'https://api.github.com/orgs/att/repos',
        headers: {
            'User-Agent': 'request',
            'Authorization': token
        }
    })
}

export function requestBodyOfGettingIssues(name) {
    return({
        url: `https://api.github.com/repos/att/${name}/issues`,
        headers: {
            'User-Agent': 'request',
            'Authorization': token
        }
    })
}

export function requestBodyOfGettingIssueComments(url) {
    return{
        url: url,
        headers: {
            'User-Agent': 'request',
            'Authorization': token
        }
    };
}